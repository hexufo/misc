This repository contains miscellaneous scripts:

- adatube: get the latest video titles and links from the
  adafruit youtube channel.
- cmdfoo: search commandlinefu.com database from the shell using the api.
- cmdfoornd: get a random command from commandlinefu.com.
- eevblogtube: get the latest video titles and links from the
  eevblog youtube channel.
- elilivetube: get the latest video titles and links from the
  "eli the computer guy live" youtube channel.
- elitube: get the latest video titles and links from the
  "eli the computer guy" youtube channel.
- elnmore: get the latest video titles and links from the
  "electronics and more" youtube channel.
- expandurl: expands shortened urls using the longurl.org api.
- getexternalip: get the external ip randomly from a list of web sites.
- lqlatest: get the latest threads from linuxquestions.org.
- lqnews: get the latest news from linuxquestions.org.
- miaoudate: miaous the current date !
- miaoutime: miaous the current time !!
- thinklightflash: flash the IBM X41 Thinklight n times.
- xkcdrnd: get a random comic from http://xkcd.com/ !

Get the latest versions here:
https://github.com/hexufo/misc

Project page on PyPI: https://pypi.python.org/pypi/misc-scripts

You can find me in various places on the internet:
hexufo @ {wordpress, twitter, github, gmail}

- This software is distributed under the Unlicense:

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org>
